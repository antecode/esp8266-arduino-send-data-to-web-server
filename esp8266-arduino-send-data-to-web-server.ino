#include <SoftwareSerial.h>

String ssid ="wifi_ssid";

String password="wifi_password";

SoftwareSerial esp(2, 3);// RX, TX

String server = "213.XX.XX.XX"; // REPLACE THIS WITH YOUR HOST

String uri = "/path/to/your/file.php";// our example is /esppost.php

void setup() {

esp.begin(115200);

Serial.begin(9600);

reset();

connectWifi();

}

//reset the esp8266 module

void reset() {

esp.println("AT+RST");

delay(1000);

if(esp.find("OK") ) Serial.println("Module Reset");

}

//connect to your wifi network

void connectWifi() {

String cmd = "AT+CWJAP=\"" +ssid+"\",\"" + password + "\"";

esp.println(cmd);

delay(4000);

if(!esp.find("OK")) {

Serial.println("Connecting!");

}
Serial.println("Connected!");

}
void loop () {

httppost();
delay(1000);

}

void httppost () {

esp.println("AT+CIPSTART=\"TCP\",\"" + server + "\",80");//start a TCP connection.

if( esp.find("OK")) {

Serial.println("TCP connection ready");

} delay(1000);

String postRequest ="GET //path/to/your/file.php?column1=100&column2=100&column3=100&column4=100\rHTTP/1.1\r\nHost: 213.201.143.89\r\n\r\n";

String sendCmd = "AT+CIPSEND=";//determine the number of caracters to be sent.

esp.print(sendCmd);

esp.println(postRequest.length() );

delay(500);

if(esp.find(">")) { Serial.println("Sending.."); esp.print(postRequest);

if( esp.find("SEND OK")) { Serial.println("Packet sent");

while (esp.available()) {

String tmpResp = esp.readString();

Serial.println(tmpResp);

}

// close the connection

esp.println("AT+CIPCLOSE");

}

}}
