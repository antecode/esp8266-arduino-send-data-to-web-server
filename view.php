<html>
<head>
<meta http-equiv="refresh" content="5">
</head>
<body>
<style>
#c4ytable {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
}
 
#c4ytable td, #c4ytable th {
    border: 1px solid #ddd;
    padding: 8px;
}
 
#c4ytable tr:nth-child(even){background-color: #f2f2f2;}
 
#c4ytable tr:hover {background-color: #ddd;}
 
#c4ytable th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    background-color: #00A8A9;
    color: white;
}
</style>
 
<?php
$host="213.xx.xx.xx";
$username="user";
$password="password";
$db_name="db_name";

$conn = mysqli_connect("$host","$username","$password","$db_name");

// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }
  else{
    echo '<div id="cards" class="cards">';
    echo "<TABLE id='c4ytable'>";
    echo "<TR><TH>Date</TH><TH>Time</TH><TH>Column1</TH><TH>Column2</TH><TH>Column3</TH><TH>Column4</TH></TR>";
    $result=mysqli_query($conn, "SELECT * FROM table_name ORDER BY Date DESC");
    $count=mysqli_num_rows($result);
    if($count>=1){
        while($row=mysqli_fetch_array($result)) { 
            echo "
        <tr>
        <td>".$row["date"]."</td>
        <td>".$row["time"]."</td>
        <td>".$row["column1"]."</td>
        <td>".$row["column2"]."</td>
        <td>".$row["column3"]."</td>
        <td>".$row["column4"]."</td>
            </tr> ";
        }
    }
    else
    {
        echo "
            <tr>
            <td colspan='3'><b><i>No data!!!</i></b></td>
            </tr>
        ";
    }
  }