# Esp8266 Arduino Send Data To Web Server

Esp8266 Arduino Send Data To Web Server via MySQL/PhP
- Interface esp8266 to arduino link how to: https://www.instructables.com/id/Arduino-UNO-ESP8266-WiFi-Module
- Download and install softwareserial library (if not installed)
- Edit esp8266-arduino-send-data-to-web-server.ino with correct wifi credentials
- upload code to arduino uno
- Prepare a db with table "table_name" with 6 columns
	date
	time
	column1
	column2
	column3
	column4
- Edit with correct config credentials and place file.php on your webserver (htdocs for xampp users)
- Optional: Display database data by placing view.php file on webserver and access it via browser
